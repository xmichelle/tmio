# Threatmodel.io Website 
https://threatmodel.io

This is the repository for the Threatmodel / Nothing To Hide website, made using [Hugo](https://gohugo.io).
Pull requests are welcome.

## General Guidelines
The website is focused on privacy related issues so posts must be on these topics.
We are open to a variety of styles of posts, tutorials, recommendations, formal rants, informative and thought provoking articles.
Please keep the content civil and do not masquerade theory as fact, citations and links to other relevant resources are encouraged.
Posts with affiliate links or baseless shilling will not be accepted.
Any attempt to promote or intentionally enable illegal activity is not welcome here, privacy is a human right, not just something for criminals.

## Submitting Posts
To submit your post, just make a pull request to the master branch of this repository and it will be reviewed by the community before being merged.
If you cannot do this yourself, you can ask somebody else in the community to do it for you.
The best place to get in contact with the community is our Matrix group, [#privacy:threatmodel.io](https://matrix.to/#/#privacy:threatmodel.io).
People there will be happy to help you if you have any questions or want to contribute.
