+++
title = "Privacy Pam Stickers"
date = 2020-06-16T22:48:49-04:00
draft = false
author = "michelle"
tags = ["assets"]
categories = ["resources"]
skip = true
stickersfile = true
+++

[download zip file](/stickers/privacypam.zip) (1.6MB)
  
Stickers created by Diego S. please consider sending a small Monero donation his way: 
{{< smalltext 43diegonHM4h9uPNwk6pJvFvP5BhVdohr9Bk7JikWfo4GfXLA4fMncQUS8JLijqG82jL1YBbZZikLagPWXTsQk95TXGiKCb >}}

