---
title: "How to Contribute to this Website"
date: 2020-06-24T12:43:08-04:00
author: "@michelle:threatmodel.io"
categories: ["guides"]
tags: ["contributing"]
skip: true
---

[git]: https://git-scm.com "version control system"
[hugo]: https://gohugo.io "static website system"
[gitlab]: https://gitlab.com "repository hosting platform"
[markdown]: https://daringfireball.net/projects/markdown "simple declarative markup"
[repo]: https://gitlab.com/xmichelle/tmio "ThreatModel website repository"

What do I need in order to do this?
-----------------------------------

We welcome community-created content, and the goal of this guide is to explain how to make that happen. It may require some effort to learn a new thing or two, but hopefully by the end of this you will be able to submit your content for approval in a way that makes it easy for us to include it in the website. 

You will need some software: [git][git], [hugo][hugo] and a text editor. 
You also need to create an account on the [Gitlab][gitlab] site. 
Finally, you should learn a few of the basics of [markdown][markdown]. 

Git is the version control software that is used to manage the source files that our website is generated from. 
It's not neccessary to learn many commands. 
We'll walk through fetching a copy of the repository, saving your additions and pushing those changes back up to Gitlab using Git.

Hugo is the content management system that we are using for this website. 
The advantage of using Hugo is that it is simple to use for static websites. 
You only need to be able to add a new post, edit the *front matter*, and run the server to view it locally. 
We'll walk through these as well.

Markdown is the declarative markup that Hugo converts into HTML, and it is much easier to write.
You only need to learn the most basic formatting syntax to make your post presentable.

Once you have Git, Hugo and a text editor installed, an account at Gitlab, and some privacy-related topic in mind to write about, you are ready to get started.

Creating a New Post
-------------------

Here are the steps you will take to create a new post, let's go through them one by one.

1. [Fork the repository](#forkrepo)
2. [Create a new post](#createpost)
3. [Commit your changes, and push them up to Gitlab](#commitandpush)
4. [Submit a merge request](#submitrequest)
5. [Watch for comments, questions, feedback](#wait)


{{< anchorfragment "forkrepo" >}} 
#### Fork the Repository

A repository is a collection of files; in this case it is the files that are used to generate our website. 
Forking is a process of making a copy of that repository. 
You want to create a copy and make changes to that copy, so you can submit a request to merge those changes into the main website code.

To fork the repository, log into your Gitlab account and go to the [ThreatModel.io repository][repo].
It should look like this, with a __Fork__ button on the top right.

{{< showimage imagename="guides/contributing/fork-button.jpg" title="Fork button is located in the top right">}}

Next you should download it to your machine to work on it. 
Look for the __Clone__ button and copy the HTTPS link to your clipboard.

{{< showimage imagename="guides/contributing/clone.jpg" title="Copy one of these URLs to your clipboard to clone this repo" width="438" >}}

Use this to download the repository to your computer so you can work on it. 
Use the *clone* feature in a graphical Git client, or from the command line, type  
`git clone https://gitlab.com/xmichelle/tmio.git`

{{< showimage imagename="guides/contributing/git-clone.jpg" title="Clone the repository" >}}


{{< anchorfragment "createpost" >}} 
#### Create a New Post

Hugo allows you to create a new post by using a template. 
To make a new post means creating a new __markdown__ file, which will use the __.md__ extension. 

Create your new file in the `posts` directory as shown below, and when you go to edit it you will find it in `content/posts/`. 
Use the following command to create the file, replacing "*my-privacy-post*" with a name that describes your topic, then use your text editor to write your content.

{{< showimage imagename="guides/contributing/new-post.jpg" title="Create a new post with Hugo" >}}

When you open your file in a text editor the first thing you will notice is that section at the top called *front matter*. 
The *front matter* begins and ends with three minus signs or dashes, and everything in between is meta data about your file. 
Be sure to edit the **title** field to provide what will become the heading for your post.

{{< showimage imagename="guides/contributing/edit-post.jpg" title="this is what your post looks like" >}}

You can view your progress as you continue to work on it by running `hugo server`. 
This will compile the most recent saved copy of files into memory so you can view them.

{{< showimage imagename="guides/contributing/hugo-server.jpg" title="running Hugo server" >}}

Hugo server will allow you to view your work in progress on localhost port 1313 (default) in your web browser.

{{< showimage imagename="guides/contributing/view-in-browser.jpg" title="view your work-in-progress" >}}

OK, now is when you write your masterpiece. This is why you're here.


{{< anchorfragment "commitandpush" >}} 
#### Commit Changes, Push to Gitlab

Now you're done writing and ready to submit your post, so you will need to `git add`, `git commit` and `git push` your changes. 
If you know how to do this already, skip to the next section.

Git wants to have a username and email, along with the remote URI to push code to and fetch it from. 
Gitlab is going to make sure your username and email match what you used to signup there. 
You can set the *user* properties *name* and *email* globally or locally with something like `git config --global user.name "fred"`

You will also need to tell Git where exactly to put the great post you've written. 
This is specified by setting `git remote` with the URL of this project in your Gitlab account, and the alias of `origin` that you will use to refer to this remote endpoint in future.
`git remote add origin https://gitlab.com/<your_username>/tmio.git`

{{< showimage imagename="guides/contributing/git-setup.jpg" title="Configure git" >}}

Before you push your changes up to Gitlab however, you need to have Git omclude them in the repository. 
Typing `git status` shows you your file and shows it as untracked, and is typically displayed in red. 

So to tell Git that this should be included in the next commit, you want to do `git add <filename>`, and when you check the status again it should be green and listed in the section of files that are staged and ready to commit.
Committing files in Git is as simple as `git commit` but you need to add a comment describing the changes in this commit. 
`git commit -m "added a post about a new privacy service I found"`

{{< showimage imagename="guides/contributing/commit-changes.jpg" title="Commit changes to repository" >}}

Now you're ready to send these changes to Gitlab, and this is done with `git push origin main` where origin is the name you assigned to the Gitlab remote earlier, and `main` is the name of the branch to send. 
This assumes you did not create a new branch and not merge it, which is fine also. 
If you did that, be sure to specify that branch also when you're on the Gitlab site finishing the process of submitting your merge request.
Let's do that now.


{{< anchorfragment "submitrequest" >}} 
#### Submit Merge Request

Go to the Gitlab website and verify that your fork of the Threamodel.io website has been updated with your new post. 
If it looks good, navigate to the original TMIO project you forked from, in order to submit your merge request. 
You'll find the button to click located on the left side menu.

{{< showimage imagename="guides/contributing/new-request.jpg" title="click to begin submitting your merge request" >}}

There are two forms in this process, and this first one simply asks you to choose which source repository and branch you want to have merged into which target repository and branch. 

{{< showimage imagename="guides/contributing/submit-request.jpg" title="Verify the branch names to merge" >}}

This will bring you to a form to fill out which is also important, so be sure to take some care here. 
Pay particular attention to the __description__ field. 
The two main goals here are to describe the nature of the post to be added, and to provide any additional information that might be needed related to this post. 

{{< showimage imagename="guides/contributing/merge-request.jpg" title="Fill out the form to submit your merge request" >}}

Click the green *Submit Merge Request* button when you're ready to submit your post. 
Congratulations, you wrote and submitted a post to the ThreatModel.io website!


{{< anchorfragment "wait" >}} 
#### Victory Lap

You're done for now, but keep an eye out for any feedback, or suggestions, or requests or notice that it was included in the website. 
While you're waiting, feel free to write another, and by the way - thanks!

