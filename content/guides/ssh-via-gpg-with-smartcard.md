+++
title = "Authenticating SSH via GPG on a Yubikey / Security Key"
date = 2020-06-19T00:00:00-00:00
author = "@logan:threatmodel.io"
draft = false 
tags = ["GPG", "authentication", "SSH", "Yubikey"]
categories = ["guides"]
skip = true
+++

# Part 2: Authenticating SSH via GPG on a Yubikey / Security Key 

## What is SSH? 

SSH stands for "Secure Shell", a secure network protocol designed by [Tatu Ylönen](https://ylonen.org) to replace Telnet way back in 1995.

It is most often used to authenticate (login) to remote servers, get a command-line on, and copy/retrieve files.

If you're here because you want to *Use SSH with Yubikey*, Nitrokey, or any other smartcard, this article was made for you, too. 

The instructions in this guide should work on most recent Linux distros, but were written for Debian derivatives (Ubuntu, Whonix, Mint, Kali) and tested on Debian 10.

## Requirements

1. As this guide builds upon skills and steps completed in [Part 1: Creating a GPG key for use with a Yubikey / Security Key](../gpg-with-smartcard/ "gpg-with-smartcard") . It assumes you already have a smartcard that has been loaded with a SSH or GPG key.
2. 
You have a root or sudo account on a remote Linux host via SSH.
3. Alternatively, a [Github](https://www.github.com), [Gitlab](https://www.gitlab.com), or [Gitea](https://www.gitea.io) account you would like to push to via `git` on the command line. If this is you, please follow **Step 3a** for Git* instructions below.




## Notes and Recommendations

* Most USB ""Security Keys" contain OpenPGP/GnuPG smartcard *applets*. These terms are not, however, mutually exclusive.


* We use nano as an editor in these examples. Don't hesitate to swap it for emacs, vi(m), or your editor of choice.


## Step 1:
Create or edit the following file:

	$ sudo nano ~/.gnupg/gpg-agent.conf

Add this line:
`enable-ssh-support`

## Step 2:
### Make `gpg-agent`accept SSH-keys:

Add the ssh variable:

	$ SSH_AUTH_SOCK=/run/user/1000/gnupg/S.gpg-agent.ssh

Then:
	
	$ export SSH_AUTH_SOCK

If this step was successful, you should be able to generate an `ssh-rsa` public key from your card with:

	$ ssh-add -L  

Copy this output to the clipboard `CTRL+SHIFT+C`
## Step 3:

### Add your SSH-key to the host:

Log into your SSH server as you usually do:

	$ ssh user@<ip_address> -p <port_number_if_needed> 

Now, open the `authorized_keys` file:
	
	$ sudo nano  ~/.ssh/authorized_keys:

Paste the key you copied to your clipboard in the last step on a new line*. 

**Privacy Tip: The end of the string you have pasted is signified by ==. Any data after this is a comment. An entry such as “cardno:000810346017” is a unique identifier for your smartcard and can be safely removed for privacy's sake, if you wish to do so.*

Save and exit. 
  
 Restart the SSH daemon:
	
	$ sudo systemctl restart ssh

## Step 3a:

If you have been following this guide to authenticate with [Github](https://www.github.com), [Gitlab](https://www.gitlab.com), or [Gitea](https://www.gitea.io), please log in and paste the key in the corresponding section under your account/profile. This can usually be found under "*add SSH-keys*" or similar. After this, follow the following guide to login / make code pushes: [Testing your SSH connection - GitHub Help](https://help.github.com/en/github/authenticating-to-github/testing-your-ssh-connection)





## Step 4:
Authenticate!

	$ ssh you@yourserver.com

If you are successful, you should receive a prompt asking for your smartcard PIN. Once entered it should drop you to a shell prompt. 

Congratulations: You are in!

---

**Bonus: ** 
If you access this server often, you may wish to setup ssh aliases for convenience. You can setup something simple like `ssh 1` to login:

Create or edit the following file:   
	
	$ sudo nano ~/.ssh/config

Enter an alias using the following syntax, replace the square brackets with your information. 

**Tip:** Don't enter these square brackets in the actual file.

	Host <your_alias>
	    HostName <ip_address/domain_name>
	    User <username>
	    Port <port_number>


## Addendum:

If you have more than one Yubikey / Smartcard you may wish to use them interchangeably / as a backup in the event of loss. 

You *can* do this, but it requires manual configuration via `udev`that is, unfortunately, beyond the scope of this guide. 

You can follow the steps as outlined here in this Stack Exchange article: 
[How to use multiple smart cards with GnuPG](https://security.stackexchange.com/a/204085)

---
Congratulations! This is the end of the series....for now. 

You should now be ready to effortlessly use SSH with your Yubikey. Please visit and bookmark [Threatmodel.io](https://threatmodel.io) for the sequel to this series or say hi to us on [Matrix](https://matrix.to/#/#privacy:threatmodel.io), a federated communications platform we endorse and support.

Logan
 
