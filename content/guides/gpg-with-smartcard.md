+++
title = "Preparing a Yubikey for GPG"
date = 2020-06-18T00:00:00-00:01
author = "@logan:threatmodel.io"
draft = false 
tags = ["GPG", "authentication", "SSH", "Yubikey"]
categories = ["guides"]
skip = true
+++

# Part 1: Creating a GPG key for use with a Yubikey / Security Key

  
## What is PGP?   
  
PGP stands for "Pretty Good Privacy", an encryption program designed by Phil Zimmerman in 1991 for encrypting, signing, and authenticating digital documents.   
  
The well known implementation of PGP is GPG, aka GnuPG (*GNU Privacy Guard*), a reference implementation of the OpenPGP standard.

Although many tech enthusiasts, coders, and privacy advocates have heard of PGP, many have tried it with disappointing results, or have been alienated by the notoriously convoluted interface. This is, however, no comment on the usefulness or ubiquity of this as-of-yet unbroken cryptography tool. 
    
Anyway, despite the reputation that precedes it, our multi-part guide aims to put you on the path of least resistance with PGP; One where you will learn painless ways of doing the most useful tasks with PGP.

This first part focuses on key creation and loading it onto your security key. 

Future articles will focus on such topics as:  
  
  + Authenticating to an SSH server with your GPG key
  + Signing software, documents, and other media
  + Encrypting entire volumes of data (including hard disks)
  + The "Web of Trust", importing, and signing 3rd party keys
  + How to use public keyservers to distribute and retrieve keys
  + A public key cryptography primer.

## Requirements
First, a few assumptions have been made for continuity in this guide:

1. You are running Linux - All modern Linux distros come with gpg2 pre-installed as part of the GNU core tools. Testing was done on Debian based machines, but should work on all.

2. You are comfortable opening and using a terminal / command line and have superuser (su/sudo) access. A beginner-intermediate level should be enough to get you through. There are indeed graphical front-ends available for GPG, but are out of the scope of this guide.   

3. You own a device conforming to the OpenPGP smartcard standard. Most USB security keys like Yubikey and Nitrokey include a built-in smartcard feature. 
[Wikipedia: ISO/IEC 7816-4, -8](https://en.wikipedia.org/wiki/OpenPGP_card)  
  
## Notes and Recommendations
  
* The `gpg` or `gpg2` command can both be used for our purposes. 

* The terms “secret key” and “private key” are used interchangeably within all parts of this guide.

* You may come to realize that it *is* possible to generate a GPG key natively on your smartcard, but this is not recommended: If you use this method it is impossible to backup or duplicate the key on another smartcard. In the event of loss, you will *lose control of the key forever*. 
 
* For keys generated on a PC: you *must* make a backup copy immediately after creation. Transferring a key to a smartcard is a destructive process, leaving your keys as "stubs" in the keyring, which are virtually useless references to the key that is only "sort of" on your smartcard. 

* Smartcards dont actually hold the key itself, but rather a cryptographic alias of it. Again, if you don't back it up before transferring to the smartcard, you'll be out of luck if you ever lose it.
  
* If you ever get stuck, feel free to read the gpg manual by typing “man gpg2” at the command line if you are interested in learning more about the tool.

* Get a good password manager and learn to sign in with 2FA (Preferably using your Yubikey or Nitrokey with FIDO U2F). This will help you store passphrases and PINs before proceeding with this guide. We recommend Bitwarden, an audited free and open source project. [https://www.bitwarden.com](https://www.bitwarden.com). 

* Depending on your threat model, consider printing your private keys on a piece of paper that will be stored securely in a safe and somwhere outside of home like a safety deposit box or a false floor in your grandma's house. Be sure to never store the passphrase for the key in the same physical location. Leave a meaningful hint for yourself as to what it is, but don't write "LOGAN'S SUPER SECRET KEY" explicitly, for obvious reasons. A USB stick containing your private key can perform a similar function. 

**Security Note:** 

It's likely that the PC you'll be using to generate your new key has a live connection to the internet. An ideal environmant for key generation, however, is one without any connectivity (ideally no network card at all). This type of host is referred to as an “air-gap”, or "air-gapped PC". Using this method largely decreases the chance that your key could be stolen / exfiltrated. Unfortunately, though, we realize that this isn't practical for most people. 

Instead, in the absense of a dedicated air-gapped host, we recommend using a virtual machine (VM) without networking. It is, unfortunately, beyond the scope of this guide, but there exist endless how-tos for installing linux on a virtual machine on the internet. The safest, quick and dirty solution would be to download a ready-made Whonix VirtualBox appliance for this purpose and run it on VirtualBox software. The Whonix site has enough information to get you started. 

[https://www.whonix.org/wiki/VirtualBox/XFCE](https://www.whonix.org/wiki/VirtualBox/XFCE) 

Good luck!


## Step 1:

GPG, or GnuPG, is a full suite of tools allowing you to create, modify and edit personal keys, import and trust keys from 3rd parties, and manage the inventory via a keyring.   
  
You can view your keyring at any time by typing `gpg2 --list-keys`. Let's do that now. The first time you run the command, you'll notice a new database is created for you:.

		user@host:~$ gpg2 --list-keys
		gpg: /home/user/.gnupg/trYustdb.gpg: trustdb created
Now let's Install `scdaemon` (smartcard daemon) to ensure your system can read your card properly:

`sudo apt install scdaemon gnupg-agent`

Then, insert your card and confirm that you can view details about it: 

`gpg2 --card-status`

	user@host:~$ gpg2 --card-status
	Reader ...........: OpenPGP Smartcard FIDO CCID
	Application ID ...: D2304957823409648347234087523407
	Version ..........: 3.4
	Manufacturer .....: Developer
	Serial number ....: 39716002
	Name of cardholder: Logan Banks
	Language prefs ...: [not set]
	Sex ..............: unspecified
	Pronouns..........: dev/null
	URL of public key : threatmodel.io
	Login data .......: logan
	Signature PIN ....: forced
	Key attributes ...: rsa4096 rsa4096 rsa4096
	Max. PIN lengths .: 127 127 127
	PIN retry counter : 3 3 3
	Signature counter : 0
	KDF setting ......: on
	Signature key ....: E21C 1E73 E99E 6F23 DACA  A8B5 E6FB 4619 A4F2 713A
	      created ....: 2020-04-25 13:24:19
	Encryption key....: 818A 68A5 82B3 8BD6 F326  22C4 7734 0B4C 4480 DBC8
	      created ....: 2020-04-21 20:03:27
	Authentication key: FCA0 5B73 EDA8 2E3A 8363  57AC 96C1 3850 ED28 69A9
	      created ....: 2020-04-21 20:05:47
	General key info..: [none]



You should receive a similar printout showing your key's status/details. Take note of the `Key attributes` section where you should see one of `rsa2048` `rsa3072` or `rsa4096`. This is the highest encryption standard your smartcard supports. Remember this number now as it will be important when generating a key later. If your `Key attributes` section has a number under rsa2048 it may be insecure, and we advise getting a newer card.



## Step 2:
*Note: If you already have a private key you wish to be imported, skip to “Step 2a”. If this is your first time using a smartcard, it does not contain one yet. Private keys cannot be "exported" from a smartcard.*


### Generating your first GPG key:

Let's begin by entering the following command: `gpg2 --full-gen-key`

	$ gpg2 --full-gen-key
	
	Please select what kind of key you want:
	(1) RSA and RSA (default)	
	(2) DSA and Elgamal
	(3) DSA (sign only)
	(4) RSA (sign only)

Choose (1) here.

	RSA keys may be between 1024 and 4096 bits long.
	What keysize do you want? (3072)

Enter the number matching the `Key attributes` section of your smartcard you noted from Step 1 (eg. 4096).

	Please specify how long the key should be valid.
	0 = key does not expire
	<n> = key expires in n days
	<n>w = key expires in n weeks
	<n>m = key expires in n months
	<n>y = key expires in n years

We recommend setting 1 year for key expiry unless you will store the key backup on a device that is permanently air-gapped.
  
---
If that's you, your option is to set expiry to `0 = key does not expire` and rely on the revocation certificate we will create in Step 3 to invalidate it, should it ever become lost. 

Keep in mind that revocation *does not* work if you die or become incapacitated, of course. Revoking a key merely signals an invalidation to those users paying attention. The key is still usable and relies on users receiving updates from a public keyserver to learn about your key's new status.
 ___

The next section asks for bits of identification to help create a user ID for you. It only needs one of the following 3 fields in order to create an ID. You can enter whatever you feel will be appropriate to enter here. For many it is an e-mail address where someone wishing to confirm your identity can reach you.

	GnuPG needs to construct a user ID to identify your key.
	
	Real name:
	Email address: logan@threatmodel.io
	Comment:
	
	You selected this USER-ID:
	"(logan@threatmodel.io)"
	
	Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
	We need to generate a lot of random bytes. It is a good idea to perform
	some other action (type on the keyboard, move the mouse, utilize the
	disks) during the prime generation; this gives the random number
	generator a better chance to gain enough entropy.

GnuPG will now ask you for a passphrase. This is where you can really secure your key with a strong passphrase. If you chose a password manager (as suggested above) you should generate a long 128 character passphrase with special characters, upper + lowercase characters, and numbers. Record it in the password manager first, then enter it twice in the passphrase request prompt.

You should see something that looks like this:

	gpg: /home/user/.gnupg/trustdb.gpg: trustdb created
	gpg: key 8C87E9B8BDC644D1 marked as ultimately trusted
	gpg: directory '/home/user/.gnupg/openpgp-revocs.d' created
	gpg: revocation certificate stored as '/home/user/.gnupg/openpgp-revocs.d/	94B3C2F6650644C6771881828C87E9B8BDC644D1.rev'
	public and secret key created and signed.

	pub rsa2048 2020-05-07 [SC]
	94B3C2F6650644C6771881828C87E9B8BDC644D1
	uid (logan@threatmodel.io)
	sub rsa2048 2020-05-07 [E]


Right away, pay attention to a few important elements that we will rely on later:
The very first line starting with `pub` denotes the characteristics of the “Master” key you've created. Will soon create *sub-keys*. The `pub` (public key) just means we are viewing the public key. We can view the private key too, but it's not necessary right now.

The `[SC]` section denotes its usage (sign and certify): A master GPG key must always retain the C (certify) usage flag, but there are 3 other usage configurations you can assign to keys. They are as follow:

+ S - Signing (proves you have interacted with the media you've signed. Usually files or code)
+ E - Encryption (used for sending private e-mail or encrypting / decrypting a hard disk)
+ A - Authentication (used for logging in to systems or remote servers)

You can attach these usage flags to the master key and the relatively unlimited number of subkeys that can be created for it. We will add two more of our own shortly (for a total of 1 master key with 3 subkeys).

The final elements of this screen are the two types of identifiers we can use to call the key when doing operations on it later:

1. The key fingerprint is something that looks like this`94B3C2F6650644C6771881828C87E9B8BDC644D1` 

	This is a cryptographic hash that can only be generated from your private key and is unique to you. Older users of GPG still sometimes use the last 8 characters of a fingerprint, but it is not a best practice. It has been shown that those last 8 characters can be spoofed. You are better off using the full fingerprint to refer to your key wherever copy + paste is available.

2. The uid (user ID) is comprised of name, comments, or e-mail address. It is often seen in brackets like `<logan@threatmodel.io>`

From now on we will refer to these elements as the `<KeyID>`

### Generating Additional Subkeys:

Recall that above we have one subkey with the [E] Encryption usage flag set. We intend to add two more later: One for [S] Signing and one for [A] authentication.
If you forget what the current configuration is, you can always use

`$ gpg2 --list-keys`
To review the configuration

Let's proceed to add a new subkey:

	$ gpg2 --expert --edit-key <KeyID>
	gpg>addkey

Now we want to customize the subkey. Select 8:

	Please select what kind of key you want:
	(3) DSA (sign only)
	(4) RSA (sign only)
	(5) Elgamal (encrypt only)
	(6) RSA (encrypt only)
	(7) DSA (set your own capabilities)
	(8) RSA (set your own capabilities)
	(10) ECC (sign only)
	(11) ECC (set your own capabilities)
	(12) ECC (encrypt only)
	(13) Existing key
	Your selection? 8

Here you can toggle the capabilities so you have a subkey for signing.
Press E once to toggle Encryption off.
	
	Possible actions for a RSA key: Sign Encrypt Authenticate
	Current allowed actions: Sign Encrypt
	
	(S) Toggle the sign capability
	(E) Toggle the encrypt capability
	(A) Toggle the authenticate capability
	(Q) Finished
	
	Your selection? E


Press Q to finish.

Choose the keysize that matches your smartcard's `Key attributes`section again:

	RSA keys may be between 1024 and 4096 bits long.
	What keysize do you want? (3072) 2048
	
	Please specify how long the key should be valid.
	0 = key does not expire
	<n> = key expires in n days
	<n>w = key expires in n weeks
	<n>m = key expires in n months
	<n>y = key expires in n years
	Key is valid for? (0) 0
	Key does not expire at all
	Is this correct? (y/N) y
	Really create? (y/N) y

Enter your GPG key passphrase

Then repeat this process for a third subkey, making sure you toggle the usage for “Authenticate” using the method shown above..

Finally, when finished enter:
  
	gpg> change-usage
  
This is an unlisted command that will allow you change the usage of the primary key to "C" (certify) only since we now have a signing subkey!


Save your changes
	
	gpg> save
	

## Step 2a:

Please skip to step 3 if you have completed Step 2 and or have a newly generated GPG key.

If you have previously imported keys and they are stubs due to smartcard `keytocard` command, you can delete them before proceeding:

	$ gpg2 --delete-secret-and-public-keys <KeyID>

Confirm that they were deleted with

	$ gpg2 --list-keys	

### Importing an existing secret GPG key from file:


Notice: Make sure it's your private key file, *not* your public key. If you import a public key you won't be able to do much except trust, view, or sign it.  

	$ gpg2 --allow-secret-key-import --import <filename>
Enter your passphrase.

If successful you should see the following:

	gpg: key 8C87E9B8BDC644D1: public key " (logan@threatmodel.io)" imported
	gpg: key 8C87E9B8BDC644D1: secret key imported
	gpg: Total number processed: 1
	gpg: imported: 1
	gpg: secret keys read: 1
	gpg: secret keys imported: 1


Since you didn't create this key on this device or have previously deleted it, you should now set the key's trust level back to "ultimate"

	$ gpg2 --key-edit <KeyID>
	
	gpg>trust
	
	Please decide how far you trust this user to correctly verify other users' keys
	(by looking at passports, checking fingerprints from different sources, etc.)
	
	1 = I don't know or won't say
	2 = I do NOT trust
	3 = I trust marginally
	4 = I trust fully
	5 = I trust ultimately
	m = back to the main menu
	
	Your decision? 5
	Do you really want to set this key to ultimate trust? (y/N) y
	
	gpg>quit




## Step 3:


Securing and backing up your newly generated keys to prevent loss:

When we created the new key earlier, a revocation certificate was automatically added for us. It is found at:
	`/home/<your_name>/.gnupg/openpgp-revocs.d/<KeyID>.rev`

Let's rename it to something more memorable:

	$ cd /home/<your_name>/.gnupg/openpgp-revocs.d/
	$ mv <KeyID>.rev ~/<your_name>s_revocation_certificate.asc


*If you followed “Step 2a” and imported the private key from file, you might not have a revocation certificate. If you don't, it is recommended that you manually create one and follow the on screen instructions.

	$ gpg2 --output <your_name>s_revocation_certificate.asc --gen-revoke <KeyID>

For each of the following you will simply enter the command and your GPG key passphrase

Export the Secret Master Key

	$ gpg2 --armor --output <your_name>s_secret_master_key.sec --export-secret-key <KeyID>

Export the Secret Subkeys
	
	$ gpg2 --armor --output <your_name>s_secret_sub_keys.sec --export-secret-subkeys <KeyID>

Export the Public Master Key (aka Public Key)
	
	$ gpg2 --armor --output <your_name>s_public_master_key.asc --export <KeyID>

Please copy these four new files onto your backup media of choice for safe keeping. You can also print the private key if you have a safe place to store it as noted above.


## Step 4:

### Transferring keys to smartcard

**Remember:** this is a destructive process. If you haven't completed step 3, make sure to back up all keys before proceeding. Transferring to a smartcard will create useless “stubs” of your keys. You can't come back from this.

	$ gpg2 --edit-key <KeyID>
This will show a list of the subkeys for reference

Select subkey #1

	gpg> key 1
(notice the asterisk that appears next to they key you've chosen)
  
 Now let's move the key with the `keytocard` command:
 
	gpg> keytocard
	
	Please select where to store the key:
	(2) Encryption key
	Your selection? 2

You might see the following warning if you've ever generated or stored another key on this smartcard:

	gpg: WARNING: such a key has already been stored on the card!
	
	Replace existing key? (y/N)y
	
	Enter your GPG passphrase and then smartcard admin pin.

**Note:**
Each smartcard has two security PIN codes. The defaults are *123456* for regular PIN and *12345678* for admin pin.
Be aware that entering a pin incorrectly three times in a row will require either an unblock / admin password or a factory reset of the smartcard.
We will set permanent pins in **Step 6**.

Enter `key 1`again to deselect subkey #1
	
	gpg> key 1


Repeat this routine for key 2 and key 3 until finished. You may then

	gpg> save


**Important:** If you want to configure more than one smartcard with the same key, you must complete Step 2a, then Step 4 again for each smartcard. It is generally recommended to have at least two keys for any one identity (gpg key). Do note, however, that because of the way the ssh-agent handle card identities, SSH authentication using different smartcards loaded with the same key will not work without further, advanced configuration.


## Step 5:

### Cleaning up the keys

At this point most of our work is done. Now we want to clean up the left over files

You may wish to securely delete the secret/private key files we created in step 2 so that they are (more) unrecoverable after deletion. You may do this with:

	$ shred -u <file>



## Step 6:

### Setting up your smartcard PINs and reset codes:

*As mentioned in **Step 4**, default pins are 123456 for regular use and 12345678 for admin

Let's edit our card and change the PIN:

	$ gpg2 --card-edit
	
	gpg/card> admin
	
	gpg/card> passwd
	gpg: OpenPGP card no. XXXXXXXXXXXXXXXXXXX detected
	
	1 - change PIN
	2 - unblock PIN
	3 - change Admin PIN
	4 - set the Reset Code
	Q - quit
	
	Your selection? 1

You will be prompted for your PIN (enter default 123456)
Now enter your new (regular) PIN.

Then enter an unblock PIN, admin PIN, and reset code to your taste. Be careful! If you lose these you will have to factory reset your key and complete most of this guide again!


Force Signature PIN:
Additionally you may wish to toggle the `Signature PIN -- forced` setting.

	gpg/card>>forcesig
	
	gpg/card> quit


**Remember:** If you fail to enter the correct pin 3 times you will need the unblock PIN. 3 failed *admin PIN* tries will lock the card and it will need to be reset. If no reset code is setup you will have to use a special [APDU](https://developers.yubico.com/ykneo-openpgp/ResetApplet.html) code to completely re-initialize your smartcard applet.
  
  ---

Congratulations! You are now ready for [Part 2: Authenticating SSH via GPG on a Yubikey / Security Key](../ssh-via-gpg-with-smartcard) 









