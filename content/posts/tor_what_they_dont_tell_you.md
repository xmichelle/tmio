+++
title = "Tor - What They Don't Tell You"
draft = false
date = 2020-06-07
tags = ["tor"]
+++
[qubes]: https://qubes-os.org

In the search for tools to enhance online privacy, Tor is quick to be recommended.
With those recommendations, people also often give the impression that Tor is foolproof, that by using Tor, your online activity is completely private and anonymous.
Unfortunately it is not so easy, and the effectiveness of Tor can be reduced to nothing if not used correctly.
Promoting Tor without discussing what mistakes you can make that undermine it is irresponsible because some people are looking to rely on it for their safety.
For that reason, here we will cover what one can do to maximize the benefits they get out of Tor.

# Accounts
The most obvious way to compromise your Tor browsing is to log into an account that you have previously used without Tor.
Even if you didn't explicitly give any personal information when signing up for the account, you did give away your IP address which can be used to effectively identify you.
If a website knows that you own an account, logging into it while using Tor does nothing to conceal your identity.
Note that this still holds true when using hidden services too, Facebook still knows who you are when you log in using their .onion address.
The only reason Facebook provides it in the first place is so users can get around national firewalls that block their website.

You may be wondering, what about logging into accounts that were made while using Tor and only ever accessed using Tor?
While the websites you log into with these accounts may not know your real identity, they still know that all activity on the account is from the same person, allowing them to piece information together between Tor sessions.
Tor works best when every user looks the same because that limits an adversary's ability to accredit different activity to any single Tor user.
However by using the same account between Tor sessions, all activity in those sessions are trivially accredited to a single person.
Yes, they don't necessarily know who that single person is, but at any point in the future they can find out, revealing the identity of the person responsible for all past activity on that account.
With this in mind, it is recommended to not use any accounts at all while using Tor if possible, which I understand isn't always the case.

# Behavioral Analysis
Assuming you don't reveal your identity with an account, you must still be careful not to give away too much identifying information.
With just a few pieces of personal information, an adversary can reduce the possible identity of a Tor user from being one of the millions of Tor users to one of a handful.
For example, revealing your favorite baseball team might suggest you live in the United States, reducing the size of the anonymity pool from a few million to a few hundred thousand.
If you later complain about the rain, that pool is further reduced to Tor users that live in places in the United States that saw rain that day.
Finally, if you later talk about a wedding that you attended recently, that reduces the anonymity pool to Tor users that live in a place in the United States that saw rain on a certain day that have connections to people that had a wedding within a certain time frame.
As you can imagine, that probably doesn't leave many people, the pool of millions reduces rapidly if you aren't careful.

Another thing that reveals information about you is the way you use language.
This can be as trivial as using an American spelling of a word over a British spelling or using slang that is specific to a certain part of the world.
Although it is a good idea to try and standardize the language you use while using Tor, with recent advances in machine learning algorithms, it is becoming more and more difficult to convincingly alter your language.
These algorithms use your grammar patterns, vocabulary frequency, punctuation style, among other things to give you a unique fingerprint, making it incredibly difficult to fool them.

# Downloads
Sometimes people need to download files while using Tor but this can lead to revealing their real identity if not careful.
Many file formats, notably PDFs and the formats used in popular word processors, can trigger a connection to the internet when opened.
You may initially think that this isn't a problem because you are using Tor, but in many cases that isn't actually true.
If you are using the Tor browser, only your browser traffic is being tunneled through Tor, connections made by your PDF viewer and word processor are not.
This means that an adversary can ask you to download a PDF through Tor and when you open it in your PDF viewer, the file will make a connection to their computer, without Tor, giving them your IP address and consequently, your identity.
A simple way to avoid this kind of attack is to not download or open any files from Tor but if that isn't an option, the best solution is to use [Qubes OS][qubes], which allows you to easily use programs inside of temporary virtual machines that have no internet access at all.
Another option is to ensure that the programs you use to open files route their traffic through Tor, but be aware that this won't protect you from malware so is still somewhat risky.
The most important takeaway here is to be mindful of how you open files downloaded through Tor because they can easily be used to compromise your anonymity.

# Zero-Days
Everything discussed so far has been about mistakes that a user can make, and not an inherent problem with Tor.
What is important to understand is that an adversary will usually take the easiest route, which with careless users is probably going to be human error.
This not only applies to Tor, but other aspects of security such as encryption.
A misunderstanding of this is what spawns the idea that a *technically* sound tool like Tor or encryption is foolproof even though it isn't.
That being said, there are some things that can compromise your identity while using Tor, despite your diligence.

A zero-day vulnerability is a vulnerability that has not yet been fixed by the vendor, either because it hasn't been addressed, or they don't even know of its existence.
In the case of Tor, a severe zero-day vulnerability could allow adversaries to deanonymize users.
While it is possible that government agencies are secretly sitting on Tor zero-day exploits, it is arguably unlikely due to the scrutiny given to the Tor code, resulting in quick fixes of any exploits.
To help protect yourself from zero-day exploits, keep an eye on the status of Tor and make sure to immediately update it when there are important security patches.

# Traffic Analysis
Apart from zero-day exploits, there are still attacks that a powerful adversary can use to find the identity of Tor users.
One such attack is traffic analysis which, as the name suggests, is where the adversary analyzes Tor traffic in an attempt to piece together what is really happening.
For example, an adversary can observe the internet traffic of a Tor user of interest.
With this alone, they cannot see what that user is doing on Tor, the data is encrypted and the destination unknown.
However, the adversary can then observe traffic coming from Tor exit nodes and look for matching patterns.
If a Tor user sends five requests, each five milliseconds apart then two seconds later an exit node produces five requests, each five milliseconds apart, there is a good chance that this Tor user's traffic was what came from the exit node.
Of course, this kind of attack is only possible at the hands of a very powerful adversary with lots of resources, such as governments.
Even for governments, successfully finding the identity of a Tor user with traffic analysis is difficult, but with increasing network surveillance, it becomes more feasible.
