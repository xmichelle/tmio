+++
title = "Biometric Identifiers"
draft = false
date = 2020-06-07
tags = ["biometrics"]
author = "@michelle:theatmodel.io"
+++

## Authentication and Identification

Biometrics are considered a useful form of authentication these days. 
We see fingerprints and face scans being used to unlock phones and identify passengers in airports.
Is it a threat to our privacy?
This is a topic that cannot be covered in a brief post, but here is an introduction to the major themes.

Authentication refers to verifying a user as being who they claim to be.
Multifactor authentication describes a process where multiple authentication methods are used to ensure the user is who we think they are.
The traditional model of a secure system would authenticate you using a combination of:

- something you know
- something you have
- something you are
   
Fingerprints, facial patterns, iris scans, and the like are all examples of *something you are*.

Instead of simply being used for multifactor authentication, fingerprints and facial scans have gained popularity as the primary method of authenticating, because people find it convenient.
When you use your fingerprint to unlock your iPhone, the scan taken is compared to a stored value on the device.

When your face is scanned in a store, airport or in a photo posted on Facebook and used to identify you, the facial measurements are compared to entries in a database, and possibly stored.
There are privacy concerns in all these cases, but there are differences in the specific concerns.


## Types of Biometric Identification

Fingerprint readers generally require touching a sensor, and they are probably the most common form of biometrics used today.
This is mainly due to many of the best selling phones being sold with the sensors and software to billions of users around the world.

Facial recognition scans are widely used, and are used in phones and other consumer devices like smart doorbells, as well as by governments to identify or surveil people.
Facial recognition technologies tend to require fairly good quality photographs taken at relatively close range because when pushed to their limits they tend to have unacceptable error rates.

Iris scans are popular in the business world to control access to secure facilities. 
Technologies like vein mapping are starting to appear in commercial products like Apple smart watches, and are used by banks in some countries.
Voice recognition systems have been used for years in many applications, including automatic phone menu systems.

Gait analysis is another tecnique growing in popularity that uses video of people walking to determine their identity.
This technology currently requires up to a full minute of video, but can use video taken from a considerable distance in order to identify an individual.

Your DNA is the ultimate biometric identifier, and it has a vast amount of information about you and your ancestors.
You cannot avoid leaving samples of your DNA around whenever you leave your home, and people often submit samples of their DNA at medical offices for various reasons.
However, you should avoid sharing your DNA with a company for the sole purpose of letting them analyze it because companies are building huge databases of DNA samples from worldwide populations.

It is important to note that all of the commonly used methods have significant error rates, and are susceptible to hacking techniques. 
Fingerprint scanning is easily hackable, and there have been generic fingerprints produced that match large percentages of all fingerprints due to common patterns found in fingerprints.
Similarly iris scans and facial recognition systems have techniques that have proven successful in defeating them.
The point here is that all authentication systems can be defeated, and with biometric identity systems you have no options when your identifier has been compromised.


## Biometric Databases

Many nations are building or are planning to build biometric databases, or are incorporating biometric data about their citizens into existing databases.
This is a strategy with considerable risk, as governments have proven to be careless and inept in the past when it comes to safeguarding personal data.

India's Aadhaar national identity database is one such example, holding records for over one billion people.
Many government services are tied to this system, and as result there have been many documented cases of people unable to receive government services.

Databases cannot be secured over time if they are in use.
This is the case for any entity charged with safeguarding actively used data, and governments have not proven to be any more capable of protecting that data.

National databases are a terrible idea because they jeopardize that most personal information belonging to the citizens.
Once the data has been leaked it is too late to fix the situation; there is no recourse for the individuals.


## Privacy Concerns

Collection of biometric identifiers pose so many problems, many of which are specific to the situations in which they occur.
The issues can vary based on type of data, collection method, entity that is collecting the data and whether the data is collected in a private or public location.
For the sake of brevity, let's just walk through a few examples.

The main problem with using "something you are" to authenticate to your phone is twofold. 
A lot of sensitive information on your phone can be revealed by forcing a person to press their finger on the button, or by aiming the phone at their face.
Another problem with these technologies is that companies produce phones with the latest advancements, and over time they are bypassed using hacks.
If this sounds to you like other forms of authentication, think again.
If your password is compromised, you can simply change it.
If your fingerprint is compromised, you're unlikely to change it - it stays compromised.

When you submit to a facial scan in an airport, the data is compared to your record in the database, or a new record is created if you don't yet have one.
Effective systems need to upload the latest scan data to the central server in order to have current data.
Previous scan data might have been poor quality due to lighting issues, makeup, facial hair, or other environmental conditions - or perhaps you've aged since the previous data was collected.
The issues with this type of facial scan scenario are many, including error rates (most prevalent for women and minorities), access controls around who can interact with the data in the airport, security around the transmission of the data to and from the central servers, storage and access controls in the main repository, duration and method of storing current records, disposal procedures for old data and the applicable policies and more.

There is a great deal of concern over DNA databases for many reasons, but one of the most terrifying is the propspect of people creating genetic weapons based on DNA traits that would target specific ethnic groups.
One of the more mundane but likely areas of concern is that health and life insurance companies will use genetic information to refuse coverage to people with certain genetic predispositions to ailments.


## Summary

There is much more to say about this topic, and the concerns grow more pressing with each passing day.
The poor performance of passwords as an authentcation mechanism makes the convenience of biometric methods appealing.
Internet based services requiring high security are increasingly turning to "continuous authentication" which simply means collecting behavioral metrics from all available sensors in order to judge the likelihood of impersonation from moment to moment.
Stay tuned for future posts that go into more detail about biometrics and the challenges, dangers and privacy issues they raise.

